package com.example.FoxCoffee.service;

import com.example.FoxCoffee.implement.CoffeeStoreImplement;
import com.example.FoxCoffee.model.CoffeeStore;
import com.example.FoxCoffee.repository.CoffeeStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoffeeStoreService implements CoffeeStoreImplement {

    @Autowired
    CoffeeStoreRepository coffeeStoreRepository;

    @Override
    public CoffeeStore findbyID(int id) {
        return coffeeStoreRepository.findById(id);
    }
}
