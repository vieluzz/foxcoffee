package com.example.FoxCoffee.service;

import com.example.FoxCoffee.model.Account;
import com.example.FoxCoffee.model.CoffeeStore;
import com.example.FoxCoffee.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class AccountService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Account account = accountRepository.findByUserName(userName);
        String role;
        if(account==null) {
            throw new UsernameNotFoundException("User not found");
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        CoffeeStore coffeeStore = account.getCoffeeStore();
        if(coffeeStore == null) {
            role = "ROLE_MEMBER";
        }
        else {
            role = "ROLE_ADMIN";
        }
        grantedAuthorities.add(new SimpleGrantedAuthority(role));
        return new org.springframework.security.core.userdetails.User(
                account.getUserName(),
                account.getPassword(),
                grantedAuthorities);
    }
}
