package com.example.FoxCoffee.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Room",uniqueConstraints = {
        @UniqueConstraint(name = "room_unique",columnNames = {"nameRoom"})
})
public class Room {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "RoomId")
    private int roomId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "store_Id")
    private CoffeeStore coffeeStore;

    @NotNull
    @Column(name = "priceRoom")
    private int priceRoom;

    @NotNull
    @Column(name = "nameRoom")
    private String nameRoom;

    @NotNull
    @Column(name = "status")
    private Boolean status;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Room(@NotNull CoffeeStore coffeeStore, @NotNull int priceRoom, @NotNull String nameRoom, @NotNull Boolean status) {
        this.coffeeStore = coffeeStore;
        this.priceRoom = priceRoom;
        this.nameRoom = nameRoom;
        this.status = status;
    }

    Room() {

    }


    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public CoffeeStore getCoffeeStore() {
        return coffeeStore;
    }

    public void setCoffeeStore(CoffeeStore coffeeStore) {
        this.coffeeStore = coffeeStore;
    }

    public int getPriceRoom() {
        return priceRoom;
    }

    public void setPriceRoom(int priceRoom) {
        this.priceRoom = priceRoom;
    }

    public String getNameRoom() {
        return nameRoom;
    }

    public void setNameRoom(String nameRoom) {
        this.nameRoom = nameRoom;
    }
}
