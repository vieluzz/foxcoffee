package com.example.FoxCoffee.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "account", uniqueConstraints = {
        @UniqueConstraint(name = "account_unique",columnNames = {"username","email","coffeeStore_Id"})
})
public class Account implements Serializable  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "accountId")
    private int id;

    @OneToOne
    @JoinColumn(name = "coffeeStore_Id")
    private CoffeeStore coffeeStore ;

    @NotNull
    @Column (name = "username")
    private String userName;

    @NotNull
    @Column (name = "password")
    private String password;

    @NotNull
    @Column (name = "fullName")
    private String fullName;

    @Column (name = "age")
    private int age;

    @Column (name = "gender")
    private Boolean gender;

    @NotNull
    @Column (name = "location")
    private String location;

    @NotNull
    @Column (name = "email")
    private String email;

    public Account(CoffeeStore coffeeStore, @NotNull String userName, @NotNull String password, @NotNull String fullName, int age, Boolean gender, @NotNull String location, @NotNull String email) {
        this.coffeeStore = coffeeStore;
        this.userName = userName;
        this.password = password;
        this.fullName = fullName;
        this.age = age;
        this.gender = gender;
        this.location = location;
        this.email = email;
    }

    public Account(@NotNull String userName, @NotNull String password, @NotNull String fullName, int age, Boolean gender, @NotNull String location, @NotNull String email) {
        this.userName = userName;
        this.password = password;
        this.fullName = fullName;
        this.age = age;
        this.gender = gender;
        this.location = location;
        this.email = email;
    }

    public Account() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CoffeeStore getCoffeeStore() {
        return coffeeStore;
    }

    public void setCoffeeStore(CoffeeStore coffeeStore) {
        this.coffeeStore = coffeeStore;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                age == account.age &&
                Objects.equals(coffeeStore, account.coffeeStore) &&
                Objects.equals(userName, account.userName) &&
                Objects.equals(password, account.password) &&
                Objects.equals(fullName, account.fullName) &&
                Objects.equals(gender, account.gender) &&
                Objects.equals(location, account.location) &&
                Objects.equals(email, account.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, coffeeStore, userName, password, fullName, age, gender, location, email);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", coffeeStore=" + coffeeStore +
                ", username='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", location='" + location + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}