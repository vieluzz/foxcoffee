package com.example.FoxCoffee.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table (name = "coffeeStore",uniqueConstraints = {
        @UniqueConstraint(name = "coffee_unique",columnNames = {"name","location"})
})
public class CoffeeStore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "location")
    private String location;

    public CoffeeStore(@NotNull String name, @NotNull String location) {
        this.name = name;
        this.location = location;
    }

    CoffeeStore() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
