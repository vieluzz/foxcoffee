package com.example.FoxCoffee.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Bill {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "billId")
    private int billId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "store_Id")
    private CoffeeStore coffeeStore;

    @NotNull
    @Column(name = "nameBuyer")
    private String nameBuyer;

    @NotNull
    @Column(name = "priceBill")
    private int priceBill;

    @NotNull
    @Column(name = "nameBill")
    private String nameBill;

    @NotNull
    @Column(name = "number")
    private int number;

    public Bill() {

    }


    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public CoffeeStore getCoffeeStore() {
        return coffeeStore;
    }

    public void setCoffeeStore(CoffeeStore coffeeStore) {
        this.coffeeStore = coffeeStore;
    }

    public int getPriceBill() {
        return priceBill;
    }

    public void setPriceBill(int priceBill) {
        this.priceBill = priceBill;
    }

    public String getNameBill() {
        return nameBill;
    }

    public void setNameBill(String nameBill) {
        this.nameBill = nameBill;
    }

    public int getNumber() {
        return number;
    }

    public String getNameBuyer() {
        return nameBuyer;
    }

    public void setNameBuyer(String nameBuyer) {
        this.nameBuyer = nameBuyer;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "billId=" + billId +
                ", coffeeStore=" + coffeeStore +
                ", nameBuyer=" + nameBuyer +
                ", priceBill=" + priceBill +
                ", nameBill='" + nameBill + '\'' +
                ", number=" + number +
                '}';
    }

    public Bill(@NotNull CoffeeStore coffeeStore, @NotNull String nameBuyer, @NotNull int priceBill, @NotNull String nameBill, @NotNull int number) {
        this.coffeeStore = coffeeStore;
        this.nameBuyer = nameBuyer;
        this.priceBill = priceBill;
        this.nameBill = nameBill;
        this.number = number;
    }
}
