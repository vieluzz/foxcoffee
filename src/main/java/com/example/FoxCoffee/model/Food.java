package com.example.FoxCoffee.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Food",uniqueConstraints = {
        @UniqueConstraint(name = "food_unique",columnNames = {"nameFood"})
})
public class Food implements Serializable {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "foodId")
    private int foodId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "store_Id")
    private CoffeeStore coffeeStore;

    @NotNull
    @Column(name = "priceFood")
    private int priceFood;

    @NotNull
    @Column(name = "nameFood")
    private String nameFood;

    public Food(CoffeeStore coffeeStore, @NotNull int priceFood, @NotNull String nameFood) {
        this.coffeeStore = coffeeStore;
        this.priceFood = priceFood;
        this.nameFood = nameFood;
    }

    Food() {

    }
    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public CoffeeStore getCoffeeStore() {
        return coffeeStore;
    }

    public void setCoffeeStore(CoffeeStore coffeeStore) {
        this.coffeeStore = coffeeStore;
    }

    public int getPriceFood() {
        return priceFood;
    }

    public void setPriceFood(int priceFood) {
        this.priceFood = priceFood;
    }

    public String getNameFood() {
        return nameFood;
    }

    public void setNameFood(String nameFood) {
        this.nameFood = nameFood;
    }

}
