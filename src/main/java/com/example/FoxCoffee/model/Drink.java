package com.example.FoxCoffee.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Drink",uniqueConstraints = {
        @UniqueConstraint(name = "drink_unique",columnNames = {"nameDrink"})
})
public class Drink implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "drinkId")
    private int drinkId;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "store_Id")
    private CoffeeStore coffeeStore;

    @NotNull
    @Column(name = "priceDrink")
    private int priceDrink;

    @NotNull
    @Column(name = "nameDrink")
    private String nameDrink;

    public Drink(CoffeeStore coffeeStore, @NotNull int priceDrink, @NotNull String nameDrink) {
        this.coffeeStore = coffeeStore;
        this.priceDrink = priceDrink;
        this.nameDrink = nameDrink;
    }

    Drink() {

    }
    public int getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(int drinkId) {
        this.drinkId = drinkId;
    }

    public CoffeeStore getCoffeeStore() {
        return coffeeStore;
    }

    public void setCoffeeStore(CoffeeStore coffeeStore) {
        this.coffeeStore = coffeeStore;
    }

    public int getPriceDrink() {
        return priceDrink;
    }

    public void setPriceDrink(int priceDrink) {
        this.priceDrink = priceDrink;
    }

    public String getNameDrink() {
        return nameDrink;
    }

    public void setNameDrink(String nameDrink) {
        this.nameDrink = nameDrink;
    }

}
