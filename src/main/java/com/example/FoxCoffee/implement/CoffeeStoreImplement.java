package com.example.FoxCoffee.implement;

import com.example.FoxCoffee.model.CoffeeStore;

public interface CoffeeStoreImplement {
    public CoffeeStore findbyID(int id);
}
