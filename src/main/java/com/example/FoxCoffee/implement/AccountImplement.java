package com.example.FoxCoffee.implement;

import com.example.FoxCoffee.model.Account;

public interface AccountImplement {
    public Account findbyID(int id);
    public Account add();
}
