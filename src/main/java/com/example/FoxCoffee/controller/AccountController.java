package com.example.FoxCoffee.controller;

import com.example.FoxCoffee.model.*;
import com.example.FoxCoffee.repository.*;
import com.example.FoxCoffee.service.AccountService;
import com.example.FoxCoffee.service.DrinkService;
import com.example.FoxCoffee.service.FoodService;
import com.example.FoxCoffee.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class AccountController {

    @Qualifier("accountService")
    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CoffeeStoreRepository coffeeStoreRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    DrinkService drinkService;

    @Autowired
    FoodService foodService;

    @Autowired
    RoomService roomService;

    @Autowired
    FoodRepository foodRepository;

    @Autowired
    BillRepository billRepository;

    @Autowired
    RoomRepository roomRepository;

    private static boolean registered = false;

    private static boolean bought = false;

    private static String name;

    @GetMapping("/admin")
    public String admin() {
        return "quanly";
    }

    @GetMapping("/403")
    public String accessDenied() {
        return "403";
    }

    @GetMapping("/")
    public String getHome(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        name = ((UserDetails)principal).getUsername();
        return "home";
    }

    @GetMapping("/login")
    public String getLogin(Model model) {
        model.addAttribute("name",name);
        String successRegister;
        System.out.println("Asd");
        if(registered) {
            successRegister = "Đăng ký thành công";
            model.addAttribute("successRegister", successRegister);
            setRegistered(false);
        }
        return "login";
    }

    @PostMapping("/login")
    public String postLogin() {
        return "login";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("account", new Account());
        return "register";
    }

    @PostMapping("/register")
    public String postRegister(Model model,@ModelAttribute("account") Account account) {
        String usedUserNameOrEmail;
        Account accountTestUsername = new Account();
        Account accountTestEmail = new Account();
        System.out.println(5);
        accountTestUsername = accountRepository.findByUserName(account.getUserName());
        accountTestEmail = accountRepository.findByEmail(account.getEmail());
        if(accountTestUsername == null) {
            if(accountTestEmail == null) {
                account.setPassword(passwordEncoder.encode(account.getPassword()));
                accountRepository.save(account);
                setRegistered(true);
                return "redirect:/login";
            }
            else {
                usedUserNameOrEmail = "Tên đăng nhập hoặc email đã sử dụng";
                model.addAttribute("usedUserNameOrEmail", usedUserNameOrEmail);
                return "register";
            }
        }
        else {
            usedUserNameOrEmail = "Tên đăng nhập hoặc email đã sử dụng";
            model.addAttribute("usedUserNameOrEmail", usedUserNameOrEmail);
            return "register";
        }
    }

    @GetMapping("/quanly")
    public String quanlydonhang(Model model) {
        List<Bill> bills;
        bills = billRepository.findAll();
        model.addAttribute("bills",bills);
        return "quanly";
    }
//    @RequestMapping("/add")
//    public String add() {
//        Account account = new Account();
//        account.setUserName("luc123456");
//        account.setPassword(passwordEncoder.encode("123456789"));
//        account.setAge(20);
//        account.setEmail("lucqng111@gmail.com");
//        account.setFullName("Phạm Viết Lực ");
//        account.setGender(true);
//        account.setLocation("Động Thăng Thiên");
//        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
//        account.setCoffeeStore(coffeeStore);
//        return null;
//    }

    @GetMapping("/info")
    public String info(Model model) {
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        name = ((UserDetails)principal).getUsername();
        model.addAttribute("infor",accountRepository.findByUserName(name));
        String gender = "Male";
        Account account = accountRepository.findByUserName(name);
        if (!account.getGender()) {
            gender = "Female";
        }
        model.addAttribute("gender",gender);
        return "information";
    }

    @GetMapping("/thucdon")
    public String thucdon(Model model) {
        model.addAttribute("salad", foodRepository.findByNameFood("Salad Thit Nuong") );
        model.addAttribute("mixaothapcam", foodRepository.findByNameFood("Mi Xao Thap Cam") );
        model.addAttribute("supremepizza", foodRepository.findByNameFood("Supreme Pizza") );
        model.addAttribute("supcua", foodRepository.findByNameFood("Sup Cua") );
        model.addAttribute("suptom", foodRepository.findByNameFood("Sup Tom") );
        model.addAttribute("suprau", foodRepository.findByNameFood("Sup Rau") );
        return "thucdon";
    }

    @GetMapping("/thucdon/saladthitnuong")
    public String saladThitNuong(Model model) {
        model.addAttribute("salad", foodRepository.findByNameFood("Salad Thit Nuong") );
        model.addAttribute("bill",new Bill());
        if(bought) {
            model.addAttribute("bought","Đặt hàng thành công");
            setBought(false);
        }
        return "saladthitnuong";
    }

    @PostMapping("/thucdon/saladthitnuong")
    public String postSaladthitnuong(Model model, @ModelAttribute("bill") Bill bill) {
        Food food = foodRepository.findByNameFood("Salad Thit Nuong");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(food.getNameFood());
        bill.setPriceBill(bill.getNumber()*food.getPriceFood());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        setBought(true);
        return "redirect:/thucdon/saladthitnuong";
    }

    @GetMapping("/thucdon/mixaothapcam")
    public String mixaothapcam(Model model) {
        model.addAttribute("mixaothapcam", foodRepository.findByNameFood("Mi Xao Thap Cam") );
        model.addAttribute("bill",new Bill());
        if(bought) {
            model.addAttribute("bought","Đặt hàng thành công");
            setBought(false);
        }
        return "mixaothapcam";
    }

    @PostMapping("/thucdon/mixaothapcam")
    public String postMixaothapcam(Model model, @ModelAttribute("bill") Bill bill) {
        Food food = foodRepository.findByNameFood("Mi Xao Thap Cam");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(food.getNameFood());
        bill.setPriceBill(bill.getNumber()*food.getPriceFood());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        setBought(true);
        return "redirect:/thucdon/mixaothapcam";
    }

    @GetMapping("/thucdon/supcua")
    public String supcua(Model model) {
        model.addAttribute("supcua", foodRepository.findByNameFood("Sup Cua") );
        model.addAttribute("bill",new Bill());
        if(bought) {
            model.addAttribute("bought","Đặt hàng thành công");
            setBought(false);
        }
        return "supcua";
    }

    @PostMapping("/thucdon/supcua")
    public String postSupcua(Model model, @ModelAttribute("bill") Bill bill) {
        Food food = foodRepository.findByNameFood("Sup Cua");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(food.getNameFood());
        bill.setPriceBill(bill.getNumber()*food.getPriceFood());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        setBought(true);
        return "redirect:/thucdon/supcua";
    }

    @GetMapping("/thucdon/suprau")
    public String suprau(Model model) {
        model.addAttribute("suprau", foodRepository.findByNameFood("Sup Rau") );
        model.addAttribute("bill",new Bill());
        if(bought) {
            model.addAttribute("bought","Đặt hàng thành công");
            setBought(false);
        }
        return "suprau";
    }

    @PostMapping("/thucdon/suprau")
    public String postSuprau(Model model, @ModelAttribute("bill") Bill bill) {
        Food food = foodRepository.findByNameFood("Sup Rau");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(food.getNameFood());
        bill.setPriceBill(bill.getNumber()*food.getPriceFood());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        setBought(true);
        return "redirect:/thucdon/suprau";
    }

    @GetMapping("/thucdon/suptom")
    public String suptom(Model model) {
        model.addAttribute("suptom", foodRepository.findByNameFood("Sup Tom") );
        model.addAttribute("bill",new Bill());
        if(bought) {
            model.addAttribute("bought","Đặt hàng thành công");
            setBought(false);
        }
        return "suptom";
    }

    @PostMapping("/thucdon/suptom")
    public String postSuptom(Model model, @ModelAttribute("bill") Bill bill) {
        Food food = foodRepository.findByNameFood("Sup Tom");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(food.getNameFood());
        bill.setPriceBill(bill.getNumber()*food.getPriceFood());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        setBought(true);
        return "redirect:/thucdon/suptom";
    }

    @GetMapping("/thucdon/supremepizza")
    public String supremepizza(Model model) {
        model.addAttribute("supremepizza", foodRepository.findByNameFood("Supreme Pizza") );
        model.addAttribute("bill",new Bill());
        if(bought) {
            model.addAttribute("bought","Đặt hàng thành công");
            setBought(false);
        }
        return "supremepizza";
    }

    @PostMapping("/thucdon/supremepizza")
    public String postSupremepizza(Model model, @ModelAttribute("bill") Bill bill) {
        Food food = foodRepository.findByNameFood("Supreme Pizza");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(food.getNameFood());
        bill.setPriceBill(bill.getNumber()*food.getPriceFood());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        setBought(true);
        return "redirect:/thucdon/supremepizza";
    }

    @GetMapping("/dichvu")
    public String dichvu(Model model) {
        model.addAttribute("ban1",roomRepository.findByNameRoom("Ban 1"));
        model.addAttribute("ban2",roomRepository.findByNameRoom("Ban 2"));
        model.addAttribute("ban3",roomRepository.findByNameRoom("Ban 3"));
        return "dichvu";
    }

    @GetMapping("/dichvu/ban1")
    public String ban1(Model model) {
        model.addAttribute("ban1",roomRepository.findByNameRoom("Ban 1"));
        model.addAttribute("bill",new Bill());
        Room room = roomRepository.findByNameRoom("Ban 1");
        if(bought) {
            model.addAttribute("bought","Đặt bàn thành công");
            setBought(false);
        }
        if(room.getStatus()) {
            model.addAttribute("booked","Bàn đã được đặt ");
        }
        return "ban1";
    }

    @PostMapping("/dichvu/ban1")
    public String postban1(Model model,@ModelAttribute("bill") Bill bill) {
        Room room = roomRepository.findByNameRoom("Ban 1");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(room.getNameRoom());
        bill.setPriceBill(bill.getNumber()*room.getPriceRoom());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        room.setStatus(true);
        roomRepository.save(room);
        setBought(true);
        return "redirect:/dichvu/ban1";
    }

    @GetMapping("/dichvu/ban2")
    public String ban2(Model model) {
        model.addAttribute("ban2",roomRepository.findByNameRoom("Ban 2"));
        model.addAttribute("bill",new Bill());
        Room room = roomRepository.findByNameRoom("Ban 2");
        if(bought) {
            model.addAttribute("bought","Đặt bàn thành công");
            setBought(false);
        }
        if(room.getStatus()) {
            model.addAttribute("booked","Bàn đã được đặt ");
        }
        return "ban2";
    }

    @PostMapping("/dichvu/ban2")
    public String postban2(Model model,@ModelAttribute("bill") Bill bill) {
        Room room = roomRepository.findByNameRoom("Ban 2");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(room.getNameRoom());
        bill.setPriceBill(bill.getNumber()*room.getPriceRoom());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        room.setStatus(true);
        roomRepository.save(room);
        setBought(true);
        return "redirect:/dichvu/ban2";
    }

    @GetMapping("/dichvu/ban3")
    public String ban3(Model model) {
        model.addAttribute("ban3",roomRepository.findByNameRoom("Ban 3"));
        model.addAttribute("bill",new Bill());
        Room room = roomRepository.findByNameRoom("Ban 3");
        if(bought) {
            model.addAttribute("bought","Đặt bàn thành công");
            setBought(false);
        }
        if(room.getStatus()) {
            model.addAttribute("booked","Bàn đã được đặt ");
        }
        return "ban3";
    }

    @PostMapping("/dichvu/ban3")
    public String postban3(Model model,@ModelAttribute("bill") Bill bill) {
        Room room = roomRepository.findByNameRoom("Ban 3");
        CoffeeStore coffeeStore = coffeeStoreRepository.findById(23);
        Account account = accountRepository.findByUserName(name);
        bill.setNameBuyer(account.getFullName());
        bill.setNameBill(room.getNameRoom());
        bill.setPriceBill(bill.getNumber()*room.getPriceRoom());
        bill.setCoffeeStore(coffeeStore);
        billRepository.save(bill);
        room.setStatus(true);
        roomRepository.save(room);
        setBought(true);
        return "redirect:/dichvu/ban3";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    private static void setRegistered(boolean registered) {
        AccountController.registered = registered;
    }
    private static void setBought(boolean bought) {
        AccountController.bought = bought;
    }
}
