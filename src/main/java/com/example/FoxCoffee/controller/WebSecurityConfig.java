package com.example.FoxCoffee.controller;

import com.example.FoxCoffee.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public  class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Qualifier("accountService")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers("/quanly").hasRole("ADMIN")
                .antMatchers("/dichvu").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon/saladthitnuong").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon/mixaothapcam").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon/suptom").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon/supcua").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon/suprau").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon/supremepizza").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/info").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/menu").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/thucdon").hasAnyRole("MEMBER","ADMIN")
                .antMatchers("/register").permitAll()
                .antMatchers("/add").permitAll()
                .antMatchers("/greeting").permitAll()
                .antMatchers("/member").hasRole("MEMBER")
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/").hasAnyRole("MEMBER","ADMIN")
                .and()
               .formLogin()
                .loginPage("/login")
                .usernameParameter("userName")
                .passwordParameter("password")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error")
                .and()
               .exceptionHandling()
                .accessDeniedPage("/403");
    }
}
