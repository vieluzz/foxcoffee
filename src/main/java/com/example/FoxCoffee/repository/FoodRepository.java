package com.example.FoxCoffee.repository;

import com.example.FoxCoffee.model.Food;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodRepository  extends CrudRepository<Food,Long> {
    Food findByNameFood(String Name);
}
