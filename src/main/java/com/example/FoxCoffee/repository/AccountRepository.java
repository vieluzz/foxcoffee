package com.example.FoxCoffee.repository;

import com.example.FoxCoffee.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findByUserName(String username);
    Account findByEmail(String email);
}
