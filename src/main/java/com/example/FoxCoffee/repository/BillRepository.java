package com.example.FoxCoffee.repository;

import com.example.FoxCoffee.model.Account;
import com.example.FoxCoffee.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillRepository extends JpaRepository<Bill,Long> {
    Bill findByNameBill(String Name);
}
