package com.example.FoxCoffee.repository;

import com.example.FoxCoffee.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
    Room findByNameRoom(String name);
}
