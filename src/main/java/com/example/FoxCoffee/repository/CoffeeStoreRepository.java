package com.example.FoxCoffee.repository;

import com.example.FoxCoffee.model.CoffeeStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoffeeStoreRepository extends JpaRepository<CoffeeStore,Long> {
    CoffeeStore findById(int id);
}
