package com.example.FoxCoffee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoxCoffeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoxCoffeeApplication.class, args);
	}
}
